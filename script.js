//Soal 1
const Luas = (panjang, lebar) => {
  return `Panjang ${panjang} dan Lebar ${lebar}`;
}

console.log(Luas(8, 4));

//Soal 2

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }
 
// //Driver Code 
// newFunction("William", "Imoh").fullName()

const newFunction = (firstName, lastName) => {
  return {
   
    fullName: function(){
      console.log(`${firstName}  ${lastName}`)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()

//Soal 3

// const newObject = {
//   firstName: "Muhammad",
//   lastName: "Iqbal Mubarok",
//   address: "Jalan Ranamanyar",
//   hobby: "playing football",
// }

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;

// console.log(firstName, lastName, address, hobby);

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby);

// Soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
//Driver Code
const gabung = [...west, ...east];

console.log(gabung);


// Soal 5

const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

// Jawaban
var before = `Lorem  ${view}  dolor sit amet,   consectetur adipiscing elit  ${planet}`;

console.log(before);